# metodo para leer un txt desde python

with open("lorem_ipsum.txt", "r") as file:
    texto = file.read()

#contar caracteres en el texto
count_word = len(set(texto))
print(f"El número de caracteres distintos son {count_word}")

# contar palabras en el texto
list_word = texto.split()
num_words = len(set(list_word))
print(f"El número de palabras distintas son {num_words}")