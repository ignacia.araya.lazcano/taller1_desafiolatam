import sys

sol_peruano = float(sys.argv[1])
peso_argentino = float(sys.argv[2])
dolar_americano = float(sys.argv[3])
peso_chileno = float(sys.argv[4])

def conversion(sol_peruano, peso_argentino, dolar_americano, peso_chileno):
    a = peso_chileno * sol_peruano
    b = peso_chileno * peso_argentino
    c = peso_chileno * dolar_americano
    linea1 = f"los {peso_chileno} pesos chilenos equivalen a: \n"
    linea2 = f"{a} soles \n"
    linea3 = f"{b} pesos argentinos \n"
    linea4 = f"{c} dolares americanos \n"

    return print(linea1 + linea2 + linea3 + linea4)


conversion(sol_peruano, peso_argentino, dolar_americano, peso_chileno)   
