#importar metodo para tomar el valor del indice indicado
from operator import itemgetter

recordatorios = [['2021-01-01', "11:00", "Levantarse y ejercitar"],
 ['2021-05-01', "15:00", "No trabajar"],
 ['2021-07-15', "13:00", "No hacer nada es feriado"],
 ['2021-09-18', "16:00", "Ramadas"],
 ['2021-12-25', "00:00", "Navidad"]]

# Output
#print(recordatorios)

#Agregar un evento, ingreso por input o fecha fija
# fecha_evento = input("Agregue fecha del evento (AAAA-MM-DD): ")
# hora_evento = input("Agregue hora del evento (hh:mm): ")
# nombre_evento = input("Agregue nombre del evento: ")
# nueva_fecha = [fecha_evento,hora_evento,nombre_evento]
#recordatorios.append(nueva_fecha)
evento_empezar = ["2021-02-02", "18:00", "Empezar el año"]
recordatorios.append(evento_empezar)
cena_navidad = ["2021-12-24", "22:00", "Cena de navidad"]
recordatorios.append(cena_navidad)
cena_añonuevo = ["2021-12-31", "22:00", "Cena de año nuevo"]
recordatorios.append(cena_añonuevo)

#reemplazar un valor en la lista
recordatorios[2][0]= "2021-07-16"

#eliminar elemento de lista
recordatorios.pop(1)


#Ordenar lista en forma descendente con sorted(lista, key=indice a ordenar)
recordatorios_ordenados = sorted(recordatorios, key=itemgetter(0))
#print(recordatorios)
for i in recordatorios_ordenados:
    print(i)
